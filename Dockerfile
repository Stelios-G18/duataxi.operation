#  cd ..     docker build -f .\DuaTaxi.Operations\Dockerfile -t stelgio/duataxi_operation:v2 .
#            docker run -p 5004:5004 --network=duataxi-network  stelgio/duataxi_operation:v2   
FROM mcr.microsoft.com/dotnet/core/aspnet:2.2-alpine  AS base
WORKDIR /app
ENV ASPNETCORE_ENVIRONMENT docker
ENV ASPNETCORE_URLS http://*:5004
EXPOSE 5004

FROM mcr.microsoft.com/dotnet/core/sdk:2.2-alpine AS build
WORKDIR /src
COPY ["DuaTaxi.Operations/DuaTaxi.Operations.csproj", "DuaTaxi.Operations/"]
RUN dotnet restore "DuaTaxi.Operations/DuaTaxi.Operations.csproj"
COPY . .
WORKDIR "/src/DuaTaxi.Operations"
RUN dotnet build "DuaTaxi.Operations.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "DuaTaxi.Operations.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "DuaTaxi.Operations.dll"]